#include "t3board.h"

#include <iostream>
#include <cstdlib>

//using namespace std;

int main()
{
    cout << "Let's play tic tac toe!! :D" << endl << endl;

    T3Board board(8, 8 ,4);

    string piece = "X";
    bool is_over = false;
    int x, y;

    while(!is_over) {
        cout << "It's now " << piece << " turn! : ";
        cin >> x >> y;
        if(!board.inBound(x, y)) {
            cout << "\nERROR: Invalid move.\n" << endl;
            continue;
        } else if(!board.isEmpty(x, y)) {
            cout << "\nERROR: Spot taken!!\n";
            continue;
        }

        // clears screen:
        system("CLS");

        board.setPiece(x, y, piece);
        cout << piece << " has been placed in (" << x << ", "
             << y << ")\n\n";
        board.printBoard();

        vector<win_dat> winning_data = board.isWin(x, y);
        if(winning_data.empty()){
            if(piece == "X")
                piece = "O";
            else
                piece = "X";
        } else {
            for(win_dat &data : winning_data) {
            cout << piece << " completes a mark from " << data.start_tile
                 << " to " << data.end_tile << endl;
            }

            cout << "\nCongratulations, " << piece << " , you are a winner!!\n\n";
            is_over = true;
        }
    }

    return 0;
}


