#include "t3board.h"

#include <iostream>
#include <cstdlib>

//using namespace std;

int main()
{
    T3Board board(5, 5 ,4);
    board.setPiece(3, 4, "X");
    board.setPiece(3, 3, "X");
    board.setPiece(3, 2, "X");
    board.setPiece(3, 1, "X");
    board.printBoard();

    TileDat p1(1, 0), p2(2,1);
    cout << (p1 > p2) << endl;

    board.setPiece(2, 1, "O");
    board.setPiece(1, 2, "O");
    board.setPiece(0, 3, "O");
    board.setPiece(3, 0, "O");
    board.setPiece(0, 0, "O");
    board.setPiece(0, 1, "O");
    board.setPiece(0, 2, "O");
    board.setPiece(0, 4, "O");
    board.printBoard();

    vector<win_dat> winning_data = board.isWin(0, 3);

    if(winning_data.empty())
        cout << "O, you loose" << endl;
    else
        cout << "O, you win!" << endl;

    winning_data = board.isWin(3, 3);

    if(winning_data.empty())
        cout << "X, you loose" << endl;
    else
        cout << "X, you win!" << endl;

    return 0;
}

