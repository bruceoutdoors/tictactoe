#ifndef T3BOARD_H
#define T3BOARD_H

#include "boardbase.h"

// stores info when mark is reached
struct win_dat
{
    int DIRECTION;
    TileDat start_tile;
    TileDat end_tile;
};

class T3Board : public BoardBase
{
public:
    T3Board(int row, int col, int mark);
    vector<win_dat> isWin(int x, int y);
    void printBoard();
    void topOnContainer(TileDat tile, int direction,
                        vector<TileDat> &container);

    enum {
        VERTICAL,
        HORIZONTAL,
        DIAGONAL_POSITIVE,
        DIAGONAL_NEGATIVE
    };

private:
    // returns DIRECTION = -1 on failure
    win_dat getWinDat(TileDat &source, int direction);
    int win_marks;

};

#endif // T3BOARD_H
