#include "t3board.h"
#include <algorithm>

T3Board::T3Board(int row, int col, int mark)
{
    m_row = row;
    m_col = col;
    win_marks = mark;

    setupBoard();
    printBoard();
}

vector<win_dat> T3Board::isWin(int x, int y)
{
    TileDat source_point = getPoint(getIndex(x, y));
    vector<win_dat> win_container;

    // iterate through all 4 directions:
    // VERTICAL, HORIZONTAL, DIAGONAL_POSITIVE, DIAGONAL_NEGATIVE
    for(int direction = 0; direction < 4; direction++) {
        win_dat windata = getWinDat(source_point, direction);
        if(windata.DIRECTION != -1)
            win_container.push_back(windata);
    }

    return win_container;
}

void T3Board::printBoard()
{
    cout << "printing board...\n" << "     ";
    for(int i = 0; i < m_col; i++)
        cout << i << "   ";

    string h_line = "\n   -";

    for(int i = 0; i < m_col; i++)
        h_line += "----";
    h_line += "\n";

    cout << h_line;

    string piece;
    for(int i = 0; i < m_row; i++) {
        cout << i << "  |";
        for(int j = 0; j < m_col; j++) {
            piece = getPiece(j, i);
            if(piece == "")
                piece = " ";
            cout << " " << piece << " |";
        }
        cout << h_line;
    }
    cout << endl;
}

void T3Board::topOnContainer(TileDat tile, int direction,
                             vector<TileDat> &container)
{
    TileDat temp_tile = getNeighbor(tile.x(), tile.y(), direction);
    if((inBound(temp_tile.x(), temp_tile.y())) && tile == temp_tile) {
        container.push_back(temp_tile);
        topOnContainer(temp_tile, direction, container);
    }
}

win_dat T3Board::getWinDat(TileDat &source, int direction)
{
    win_dat windata;
    vector<TileDat> container;
    container.push_back(source);

    // set windata default value(failure)
    windata.DIRECTION = -1;

    if(direction == VERTICAL){
        topOnContainer(source, TileDat::TOP, container);
        topOnContainer(source, TileDat::BOTTOM, container);
    }

    if(direction == HORIZONTAL){
        topOnContainer(source, TileDat::LEFT, container);
        topOnContainer(source, TileDat::RIGHT, container);
    }

    if(direction == DIAGONAL_POSITIVE) {
        topOnContainer(source, TileDat::TOP_RIGHT, container);
        topOnContainer(source, TileDat::BOTTOM_LEFT, container);
    }

    if(direction == DIAGONAL_NEGATIVE){
        topOnContainer(source, TileDat::TOP_LEFT, container);
        topOnContainer(source, TileDat::BOTTOM_RIGHT, container);
    }

    if(container.size() >= win_marks ) {
        sort(container.begin(), container.end());
        windata.DIRECTION = direction;
        windata.start_tile = container.front();
        windata.end_tile = container.back();
    }

    return windata;
}
