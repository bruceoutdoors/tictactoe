#include "tiledat.h"

TileDat::TileDat(int x, int y, string piece) :
    m_x(x), m_y(y), m_piece(piece) {}

TileDat TileDat::operator+(const TileDat &p) const
{
    TileDat tile;
    tile.setX(this->x() - p.x());
    tile.setY(this->y() - p.y());
    tile.setPiece(this->piece());
    return tile;
}

TileDat TileDat::operator-(const TileDat &p) const
{
    TileDat tile;
    tile.setX(this->x() - p.x());
    tile.setY(this->y() - p.y());
    tile.setPiece(this->piece());
    return tile;
}

TileDat &TileDat::operator+=(const TileDat &p)
{
    *this = *this + p;
    return *this;
}

TileDat &TileDat::operator-=(const TileDat &p)
{
    *this = *this - p;
    return *this;
}

bool TileDat::operator<(const TileDat &p) const
{
//    TileDat point = *this - p; // result in error, so copy *this
    TileDat point = *this - p;

    // vertical: top > bottom
    if(point.x() == 0 && point.y() > 0)
        return true;

    // horizontal: right > left
    if(point.x() < 0 && point.y() == 0)
        return true;

    // positive diagonal: top right > bottom left
    if(point.x() < 0 && point.y() > 0)
        return true;

    // negative diagonal: top left > bottom right
    if(point.x() > 0 && point.y() > 0)
        return true;

    return false;
}

bool TileDat::operator==(const TileDat &p)
{
    // compares and return true if exact match
    if(this->piece().compare(p.piece()) == 0)
        return true;

    return false;
}

ostream & operator<<(ostream &os, const TileDat &p)
{
    // friend functions have same access rights as member fucntion
    os << "<" << p.m_piece << ">" << "-(" << p.x() << ", " << p.y() << ")";
    return os;
}
