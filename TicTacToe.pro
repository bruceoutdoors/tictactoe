#-------------------------------------------------
#
# Project created by QtCreator 2013-06-14T04:14:43
#
#-------------------------------------------------

TARGET = TicTacToe
CONFIG   += console
CONFIG   -= app_bundle
QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app

SOURCES += tiledat.cpp \
    boardbase.cpp \
    t3board.cpp \
#    test-boardbase.cpp \
#    test-T3board.cpp \
    main.cpp

HEADERS += \
    tiledat.h \
    boardbase.h \
    t3board.h
