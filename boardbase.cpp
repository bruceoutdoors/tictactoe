#include "boardbase.h"

BoardBase::BoardBase(int row, int col) :
    m_row(row), m_col(col)
{
    setupBoard();
    printModel();
}

void BoardBase::setPiece(int index, string piece)
{
    (*this)[index].setPiece(piece);
}

void BoardBase::setPiece(int x, int y, string piece)
{
    setPiece(getIndex(x, y), piece);
}

void BoardBase::movePiece(int index1, int index2)
{
    setPiece(index2, getPiece(index1));
    removePiece(index1);
}

void BoardBase::movePiece(int x1, int y1, int x2, int y2)
{
    movePiece(getIndex(x1, y1), getIndex(x2, y2));
}

TileDat BoardBase::getPoint(int index)
{
    return (*this)[index];
}

TileDat BoardBase::getNeighbor(int x, int y, int DIRECTION, int step)
{
    // return -1 if index is out of bounds
    if(!inBound(x, y))
        return TileDat();

    TileDat point = getPoint(getIndex(x, y));

    switch(DIRECTION) {
    case TileDat::TOP: point += TileDat(0, -step);
        break;
    case TileDat::RIGHT: point += TileDat(step, 0);
        break;
    case TileDat::BOTTOM: point += TileDat(0, step);
        break;
    case TileDat::LEFT: point += TileDat(-step, 0);
        break;
    case TileDat::TOP_RIGHT: point += TileDat(step, -step);
        break;
    case TileDat::BOTTOM_RIGHT: point += TileDat(step, step);
        break;
    case TileDat::TOP_LEFT: point += TileDat(-step, -step);
        break;
    case TileDat::BOTTOM_LEFT: point += TileDat(-step, step);
        break;
    }

    if(!inBound(point.x(), point.y()))
        return TileDat();

    return (*this)[getIndex(point.x(), point.y())];
}

bool BoardBase::inBound(int index)
{
    if(index < 0 || index > (this->size() - 1))
        return false;

    return true;
}

bool BoardBase::inBound(int x, int y)
{
    // coordinates must be positive, and
    // fixed by column and row
    if(x < 0 || y < 0 || x > (m_col - 1) || y > (m_row - 1))
        return false;

    return true;
}

void BoardBase::printModel()
{
    cout << "printing model..." << endl;
    for(unsigned int i = 0; i < size(); i++) {
        cout << (*this)[i] << "\t";
        if((i+1) % m_col == 0)
            cout << endl;
    }
    cout << endl;
}

void BoardBase::setupBoard()
{
    for(int i = 0; i < m_row; i++) {
       for(int j = 0; j < m_col; j++) {
            TileDat item(j, i);
            //  col = x, row = y
            push_back(item);
        }
    }
}
