#ifndef BOARDBASE_H
#define BOARDBASE_H

#include "tiledat.h"
#include <vector>
#include <string>

class BoardBase : public vector<TileDat>
{
public:
    BoardBase() {}
    BoardBase(int row, int col);

    void setPiece(int index, string piece);
    void setPiece(int x, int y, string piece);

    void movePiece(int index1, int index2);
    void movePiece(int x1, int y1, int x2, int y2);

    void removePiece(int index) { setPiece(index, ""); }
    void removePiece(int x, int y) { setPiece(x, y, ""); }

    string getPiece(int index) { return (*this)[index].piece(); }
    string getPiece(int x, int y) { return (*this)[getIndex(x, y)].piece(); }

    int getIndex(int x, int y) { return x + (y * m_col); }
    TileDat getPoint(int index);

    bool isEmpty(int index) { return (*this)[index].piece() == ""; }
    bool isEmpty(int x, int y) { return isEmpty(getIndex(x, y)); }

    // locate neighbor(index) given direction(e.g TileDat::BOTTOM_RIGHT)
    // returns empty tiledat if no neighbor match
    TileDat getNeighbor(int x, int y, int DIRECTION, int step = 1);

    // test if index is within boundaries
    bool inBound(int index);
    bool inBound(int x, int y);

    // debugging function
    void printModel();

protected:
    void setupBoard();
    int m_row;
    int m_col;
};

#endif // BOARDBASE_H
