#include "boardbase.h"

#include <iostream>

using namespace std;

int main()
{
    TileDat here(1,1, "Knight");
    TileDat there(9,0, "Queen");
    TileDat point(9,0, "Knight");
    here += there;
    cout << here << endl;
    cout << (here != there) << endl;
    cout << (here == point) << endl;

    BoardBase base(5, 3);
    base.setPiece(5, "WP");
    base.setPiece(0, "WR");
    base.setPiece(1, "WH");
    base.setPiece(2,4, "BP");
    base.setPiece(1,4, "WR");
    base.printModel();

    base.removePiece(1, 4);
    base.removePiece(0);
    cout << base.isEmpty(1, 4) << endl;
    cout << base.isEmpty(2, 4) << endl;
    cout << base.getPiece(2, 4) << endl;

    base.movePiece(2,4,1,4);
    base.printModel();
    BoardBase mybase(3,5);
    cout << mybase.inBound(5,0) << endl;
    cout << mybase.getPoint(88) << endl;
    cout << mybase.getNeighbor(14, TileDat::TOP_RIGHT);

    return 0;
}
