#ifndef POINT_H
#define POINT_H

#include <iostream>
#include <string>

using namespace std;

class TileDat
{
public:
    TileDat() {}
    TileDat(int x, int y, string piece = "");

    // getters
    int x() const { return m_x; }
    int y() const { return m_y; }
    string piece() const { return m_piece; }

    // setters
    void setPiece(string piece) { m_piece = piece; }
    void setX(int x) { m_x = x; }
    void setY(int y) { m_y = y; }

    // operator overloads:
    TileDat operator+(const TileDat &p) const;
    TileDat operator-(const TileDat &p) const;
    TileDat &operator+=(const TileDat &p);
    TileDat &operator-=(const TileDat &p);
    // operators used for sorting. Bigger TileDat floats to top left,
    // smaller to the bottom right. Priority for top.
    bool operator<(const TileDat &p) const;
    bool operator>(const TileDat &p) const { return !(*this < p); }
    // note that equality operators compare piece names not coordinates
    bool operator==(const TileDat &p);
    bool operator!=(const TileDat &p) { return !(*this == p); }
    // overides cout
    friend ostream & operator<<(ostream &os, const TileDat &p);

    enum {
        TOP = 1,
        RIGHT = 2,
        BOTTOM = 4,
        LEFT = 7,
        TOP_LEFT = 8,
        TOP_RIGHT = 3,
        BOTTOM_LEFT = 11,
        BOTTOM_RIGHT = 6
    };

private:
    int m_x;
    int m_y;
    string m_piece;
};

#endif // POINT_H
